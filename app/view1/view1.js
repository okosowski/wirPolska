'use strict';
angular.module('myApp.view1', ['ngRoute'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl',
    controllerAs: 'ctrl'
  });
}])
.controller('View1Ctrl', ['$interval', 'view1Service', function($interval, view1Service) {
  var vm = this;
  var rotatorSpeed = 4000;

  vm.data = {};
  vm.data.squares = view1Service.getDataToSquares();
  vm.data.rotator= {};
  vm.data.rotator.slides = view1Service.getDataToRotator();
  vm.data.rotator.currentIndex = 0;
  vm.data.rotator.isMouseOver = false;
  vm.data.rotator.setCurrentIndex = function (index) {
    vm.data.rotator.currentIndex = index;
  };
  vm.data.rotator.isCurrentIndex = function (index) {
    return vm.data.rotator.currentIndex === index;
  };
  vm.data.rotator.prevSlide = function () {
    vm.data.rotator.currentIndex = (vm.data.rotator.currentIndex < vm.data.rotator.slides.length - 1) ? ++vm.data.rotator.currentIndex : 0;
  };
  vm.data.rotator.nextSlide = function () {
    vm.data.rotator.currentIndex = (vm.data.rotator.currentIndex > 0) ? --vm.data.rotator.currentIndex : vm.data.rotator.slides.length - 1;
  };


  (function init(){
    $interval(function(){
      if(!vm.data.rotator.isMouseOver)
        vm.data.rotator.prevSlide();
    }, rotatorSpeed);

  })();

}]);