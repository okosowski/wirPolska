'use strict';
(function () {
    function view1Service() {

        return {
            getDataToSquares: getDataToSquares,
            getDataToRotator: getDataToRotator
        };

        function getDataToRotator(){
            return [
                {id: 0, image: './media/img/rotator_iphone_1.png', description: 'Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza'},
                {id: 1, image: './media/img/rotator_iphone_2.png', description: 'Lorem Ipsum jest tekstem... Został po raz pierwszy użyty w XV w. przez nieznanego drukarza'},
                {id: 2, image: './media/img/rotator_iphone_1.png', description: '333 333 333 333 333 333 333 333 333 333 333 333 333 333 '},
                {id: 3, image: './media/img/rotator_iphone_2.png', description: 'Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum '},
                {id: 4, image: './media/img/rotator_iphone_1.png', description: '555 555 555 555 555 555 555 555 555 555 555 555 555 555 555 555 555 555 '},
                {id: 5, image: './media/img/rotator_iphone_2.png', description: '66666666'}
            ];
        }

        function getDataToSquares(){
            return [
                {id: 0, img: 'person', title: 'Załóż konto', description: 'Zajmie Ci to2 minuty'},
                {id: 1, img: 'phone', title: 'Rób zdjęcia', description: 'Jednym kliknięciem'},
                {id: 2, img: 'exc', title: 'Dodaj filtr', description: 'Bardzo duży wybór'},
                {id: 3, img: 'order', title: 'Gotowe', description: 'Publikuj, oglądaj'}
            ];
        }
    }

    view1Service.$inject = [];
    angular
        .module('myApp.view1')
        .factory('view1Service', view1Service);
})();
