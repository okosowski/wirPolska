myApp.controller('footerCtrl', ['anchorSmoothScroll', 'footerService', function(anchorSmoothScroll, footerService) {
    var vm = this;
    vm.data = {
        twitter: footerService.getFakeTwitterDate(),
        aboutAs: footerService.getFakeAboutUsData()
    };

    vm.goToElement = function (){
        anchorSmoothScroll.scrollToTop();
    };
}]);