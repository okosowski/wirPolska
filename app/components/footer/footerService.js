'use strict';
(function () {
    function footerService() {

        return {
            getFakeTwitterDate: getFakeTwitterDate,
            getFakeAboutUsData: getFakeAboutUsData
        };

        function getFakeTwitterDate(){
            return [
                {dayAgo: '2 dni temu', content: 'Jak zrobić timeline na FB? inspirationfeed.com/inspiration/we…'},
                {dayAgo: '4 dni temu', content: 'Jak zrobić coś innego na FB'}
            ];
        }

        function getFakeAboutUsData(){
            return [
                'Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Integer Donec id elit non mi porta gravida at eget metus.',
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo.',
                'Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id ligula porta felis euismod semper. Sed posuere consectetur est at lobortis.'
            ];
        }
    }

    footerService.$inject = [];
    angular
        .module('myApp')
        .factory('footerService', footerService);
})();
