'use strict';
var myApp = angular
    .module('myApp', [
      'ngRoute',
      'ngAnimate',
      'myApp.view1'
    ])
    .config(['$routeProvider', function($routeProvider) {
    $routeProvider.otherwise(
        {redirectTo: '/view1'}
    );
  }]);

