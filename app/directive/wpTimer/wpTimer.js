(function () {
    function timer($interval) {
        return {
            restrict: 'E',
            templateUrl: function () {
                return './directive/wpTimer/wpTimer.html';
            },
            scope: {
                options: "="
            },
            link: function link(scope, element, attr, ngModelCtrl) {
                scope.vm = {};
                var ticTacInterval = 1000;
                scope.endOfToday = new Date().setHours(23,59,59,999);
                scope.toEndOfDay = tictac();

                function tictac(){
                    var currentTime = new Date().getTime();
                    var toEndOfDay = scope.endOfToday - currentTime;
                    var diffHrs = Math.floor(toEndOfDay / (60 * 60 * 1000));
                    var diffMins = Math.floor((toEndOfDay - diffHrs * 60 * 60 * 1000) / (60 * 1000));
                    var diffSecs = Math.floor((toEndOfDay - diffHrs * 60 * 60 * 1000 - diffMins * 60 * 1000) / (1000));
                    var result = [
                        {text: 'godzin', value: convertToDisplay(diffHrs)},
                        {text: 'minut', value: convertToDisplay(diffMins)},
                        {text: 'sekund', value: convertToDisplay(diffSecs)}
                    ];
                    scope.toEndOfDay = result;
                    return result;
                }

                function convertToDisplay(item){
                    if(!!item || item === 0){
                        if(item.toString().length == 1){
                            return '0' + item.toString()
                        }else{
                            return item.toString()
                        }
                    }
                }
                $interval(function(){
                    tictac();
                }, ticTacInterval);
            }
        }
    }

    timer.$inject = ['$interval'];

    angular
        .module('myApp')
        .directive('wpTimer', timer);
})();