
----------------------------- WYMAGANIA Z MAILA -----------------------------
Witam, zaczynając od maila:
1. HTML: Semantyczny kod (z wyraźnym podziałem na kluczowe elementy strony).
    zrobione

2. HTML: Optymalne osadzanie JS, CSS i webfontów oraz wstawienie odpowiednich znaczników w sekcji HEAD.
    zrobione, fonty jakie zaimportowałem to Oswald (400 i 700), oraz Open Sans(300 i 400);
    
3. JS: Aktywny minutnik symulujący miganie liczb, odliczający czas do północy danego dnia.
    zrobione

4. JS: Prosty automatyczny rotator, zatrzymujący się po najechaniu z nawigacją poprzedni/następny. 
    zrobione, trudno robi się takie rzeczy bez JSa bez jQuery,
    a dostałem info że tylko agulara mogę użyć dlatego do całego zaimporotwany jest tylko nagular
    (+route dla dobrych praktyk i aniamte)
    
5. CSS: Interaktywne elementy w sekcji „Jak to działa” – animacja na :hover (wygląd na ukrytej warstwie w PSD).
    zrobione

6. CSS: Gradienty w tle i cienie pod tekstami .
    zrobione na przykładzie stopki ("POWRÓT DO GORY")

7. Opcjonalnie CSS: Responsywny układ dla szerokości poniżej 800px, a w nim:
    pozwoliłem sobie zrobić własnego większego grida, bo tak dla dwóch rozdzielczości to dziwnie :)
    z tym że wspomniane przez państwa 800 istnieje jako $screen-sm
    tak czy inaczej wiele komponentów zachowuje swoje właściwości na bazie kilku breakPointów, nie jednego
     
Ukryta grafika telefonu
    zrobione

Wielkość tekstów odpowiednio zmniejszona lub dopasowując się do szerokości ekranu.
    wstępinie przygotowane, spradzałem na mobile i nie widziałem potrzeby zmianiać wielkości tekstu

Wszystkie elementy wycentrowane w poziomie (także w stopce).
    wycentrowałem ten które są wycentrowane na grafice

Elementy ‘Jak to działa’ mogą przechodzić do nowej linii.
    zrobione, z pośrednim stanem 2x2

Rotator nieaktywny, a wszystkie slajdy poukładane jeden pod drugim.
    rotator nieaktywny na 'hover', nie zdążyłem go poukładać

8.Opcjonalnie: wykorzystaj narzędzia jak gulp/grunt, sass/less/compass. 
    całe style w SCSSie


----------------------------- MOJE UWAGI -----------------------------
* font użyty w stopce - Helvetica Noue - kosztuje 35$ per styl, zastąpiony globalnym OpenSans
* HEADER - ukrywam nie tylko telefon ale i inne mniejsze rzeczy w zależności do rodzielczości 
* JAK TO DZIAŁA - strzałki pojawiają się tylko na dużych rozdzielczościach, animacja samym css'em
* ROTATOR - angular, w zależności od rozdzielczości znika nawigacja
* FOOTER - AKCJA WRÓC DO GÓRY - rozwiązanie zaciągniete, przeze mnie uproszczone
* FOOTER - cała reszta klasycznie RWD

PONADTO
* zdarzyć się może że podobne elementy robiłem w inny sposób, chciałem pokazać różne rozwiązania
* jako że nie dostałem grafik wektorowych na stronie przeważa rastra
* w rzeczach o podobnej logice biznesowej użyłem spritów
* gdzie tylko się dało czyściłem controlery aby nie zawierały zamokowanych tekstów,
    takie rzeczy wynosilem do Serwisów
* LICZNIK CZASU - pokazanie jak działać może dyrektywa
* JAK TO DZIAŁA i ROTATOR są zagnieżdżone na #/view1 - przykład przygotowania strony pod SPA 
* nie generowałem różnych grafik per rozdzielczości ponieważ najcięższa grafika ma 250kb (zdjęcia w rotatorze)
    a i to głównie przez to że jest .png a nie .jpg. 
    Teraz myślę, że w .png można by dać ramkę, a już zawartość ekranu telefonu mogłaby być .jpg'iem.
    (przy większej ilości slidów to byłoby chyba najbardziej optymalne rozwiązanie)
* każdorazowo dodałbym jakiś reset.css i normalize.css bo nie każda przeglądarka rozumie html5
    - jako że nie mogę nic dodawać, ominąłem ten pkt
* zminifikowałem css'y choć to mały projekt a cały mój system RWD wraz ze stylami do strony ważył 26kb,
    po minifikacji 12kb
* js'ów nie minifikowałem na wypadek gdybyście Pańśtwo chcieli się debugerem zapinać :)
* pisząc RWD, zamieściłem wiele klas który nawet jeśli nie są aktualnie wykorzystywane w projekcie,
	należało je zamieścić (jak np pełen system .span-xx-y i .offset-xx-y)


Mam nadzieję, że to co napisałem (zarówno html, js jak i scss) będzie miłą "lekturą".
Myślę, że o niczym nie zapomniałem, w każdym razie pozostaję do dyspozycji.